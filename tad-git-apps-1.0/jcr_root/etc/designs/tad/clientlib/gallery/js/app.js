$(window).ready(function () {

    $('.tabs_container li').on('click', function () {
        $(this).children('.tab-slide-box').animate({
            width: "75%",
            opacity: 1
        }, 300);
    });
    $('.tabs_container .slider-close').on('click', function (event) {
        event.stopPropagation();
        $('.tab-slide-box').animate({
            width: "0%",
            opacity: 0
        }, 300);
    });
    $('.slider').slick({
        slidesToShow: 1.7,
        slidesToScroll: 1,
        infinite: false,
        dots: true
    });
    $('.slider-bottom').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: false,
        dots: false
    });

    $(window).on('scroll', function () {
        if ($(this).scrollTop() < 50) {
            $('.header-nav').removeClass('minimized_nav');
        } else $('.header-nav').addClass('minimized_nav');
    });
});
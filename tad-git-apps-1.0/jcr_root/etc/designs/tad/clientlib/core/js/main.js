$(document).ready(function() {
    $('ul.top-link li a').click(function(){
        return false;
    });
    if ($('#heroSlider').length > 0) {
        $('#heroSlider').owlCarousel({
            loop: true,
            margin: 0,
            nav: false,
            autoplay: true,
            responsive: {
                0: {
                    items: 1
                },
                600: {
                    items: 1
                },
                1000: {
                    items: 1
                }
            }
        });
    }

    $('.hamberg').click(function() {
        $('.overlay-left').addClass("overlay-left-show");
        $('.overlay-right').addClass("overlay-right-show");
        $('.menu-wrap').addClass("menu-wrap-show");
        return false;
    });
    $("#nav-close").click(function() {
        $('.overlay-left').removeClass("overlay-left-show");
        $('.overlay-right').removeClass("overlay-right-show");
        $('.menu-wrap').removeClass("menu-wrap-show");
    });
    $('#nav ul li').each(function() {
        if ($(this).children('ul').length > 0) {
            $(this).addClass('parent');
        }
    });
    $('#nav ul li.parent > a').click(function() {
        $(this).parent().toggleClass('active');
        $(this).parent().children('ul').slideToggle('fast');
        $(this).parent().siblings().children().next().slideUp();
        $(this).parent().siblings().removeClass('active');
    });
    $('.search-btn').click(function() {
        if ($(this).parent().hasClass('active')) {
            $(this).parent().removeClass('active');
        } else {
            $(this).parent().addClass('active');
        }
        return false;
    });
    
    heroBanner();
    headerScroll();
    sliderHandler();
    sectionHeight();
});
$(window).load(function() {
    if ($('#nav').length > 0) {
        $('#nav').mCustomScrollbar({
            scrollbarPosition: "outside",
            mouseWheel: { scrollAmount: 188 },
            snapAmount: 188,
            snapOffset: 65
        });
    }
});

function heroBanner() {
    var wh = $(window).height();
    var ww = $(window).width();
    $('.heroSlider .slide-img img').css({ "height": wh + 'px' });
}

function headerScroll() {
    if ($('#heroSlider').length > 0) {
        $(window).scroll(function() {
            var scroll = $(window).scrollTop();        
            if (scroll >= 30) {
                $('#header-wrap').addClass('headerFull');
            } else {
                $('#header-wrap').removeClass('headerFull');
            }              
        });
        $('.main-container').removeClass('top-pad');
    }
    else{            
        $('#header-wrap').addClass('headerFull'); 
        $('.main-container').addClass('top-pad');
    }    
}

function sliderHandler(){
    if ($('.four-colum-slider').length > 0) {
		$('.four-colum-slider').each(function () {		
			if ($(this).find('.item').length > 4) $(this).owlCarousel({
                loop: true,
                margin: 30,
                autoplay: false,
                nav: true,
                responsive: {
                    0: {
                        items: 1
                    },
                    768: {
                        items: 3
                    },
                    1000: {
                        items: 4
                    }
                }

            });
		});
        if ($(".four-colum-slider .item").length <= 4) {		
            $('.four-colum-slider').addClass('slider-handler');
        }else{
            $('.four-colum-slider').removeClass('slider-handler');
        }
	}    
}

function sectionHeight(){
    var wh = $(window).height();	
	var ww = $(window).width();
    var whSection = wh-91;
    if(ww <= 1024){		
		$('#section-1').css({"height": "auto"});
        $('#section-2').css({"height": "auto"});
        $('#section-3').css({"height": "auto"});
        $('#section-4').css({"height": "auto"});
	}else{        
        $('#section-1').css({"height": whSection + 'px'});
        $('#section-2').css({"height": whSection + 'px'});
        $('#section-3').css({"height": whSection + 'px'});
        $('#section-4').css({"height": whSection + 'px'});
    }
    $(".scroll-button a").click(function (e) {
		var current = $(this).attr("href");
		$('html, body').stop().animate({scrollTop: $(current).offset().top-= 91}, 1000);
		return false;
	});
}
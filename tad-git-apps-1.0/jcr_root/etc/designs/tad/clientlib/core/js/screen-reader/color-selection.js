$(function () {
	
	$("#color-theme-dark").on("click", function () {
		$("body").colorChanger(customlist, $(this).attr("data-bg"), $(this).attr("data-fg"), false, "#" + $(this).attr("id"));
	});

	$("#color-theme-light").on("click", function () {
		$("body").colorChanger(customlist, $(this).attr("data-bg"), $(this).attr("data-fg"), false, "#" + $(this).attr("id"));

	});

	$("#color-theme-default").on("click", function () {
		$("body").colorChanger(customlist, $(this).attr("data-bg"), $(this).attr("data-fg"), true, "#" + $(this).attr("id"));
	});

	if (window.localStorage !== null) {
		var themeclass = window.localStorage.getItem("rajColorTheme");
		$("body").colorChanger(customlist, $(themeclass).attr("data-bg"), $(themeclass).attr("data-fg"));
	} else {
		console.log("Localstorage not available");
	}
    $('body').setFont(fontSelectors);
});


var customlist = {

	"main-column": {
		"fg": [
        ".header",
            ".footer",
        ".four-colum-slider-wrap .col-box",
            ".logo a small .h3-hd",
            ".logo a small, .logo a small .h2-hd",
            "ul.top-link li .skip-link",
			"ul.top-link li .font-incr-decr a",
            "ul.top-link li .search-wrap .search-btn .fa",
			"ul.top-link li .hamberg",
            "footer"
         ],
		"bg": [
        ".header" ,
        ".four-colum-slider-wrap .col-box",
            ".logo a small .h3-hd",
            ".logo a small, .logo a small .h2-hd",
            "footer"

        ]
	}
}

var fontSelectors = {
    "minFontSize" : 12,
    "maxFontSize" : 22,
    "p": 14,
    ".copyright" : 13,
    ".slider-content h2" : 40

  };
<%@include file="/apps/tad/common/global.jsp" %>

<footer>
        <!-- START FOOTER SECTION -->
        <div class="fbrd">
            <div class="container">
                <div class="row">
                    <div class="col-sm-4 col-md-4">

                        <div>
                            <h4><cq:include path="footer-one-text" resourceType="tad/components/content/content/text" /></h4>
                            <p><span>${base:getFormattedDateFromDate(currentPage.lastModified.time)}</span></p>
                            <div class="social-wrap">
                                <ul class="social">
                                    <cq:include path="footer-social-list" resourceType="tad/components/content/content/sociallist"/>
                                </ul>    
                            </div>
                        </div>
                    </div>


                    <div class="col-sm-4 col-md-4">
                        <div>
                            <h4><cq:include path="footer-two-text" resourceType="tad/components/content/content/text" /></h4>
                            <p>
                               <cq:include path="footer-two-richtext" resourceType="tad/components/content/content/richtext" />
                            </p>
                        </div>
                    </div>

                    <div class="col-sm-4 col-md-4">
                        <div>
                            <h4><cq:include path="footer-three-text" resourceType="tad/components/content/content/text" /></h4>
                            <p>
                               <cq:include path="footer-three-richtext" resourceType="tad/components/content/content/richtext" />
                            </p>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <div class="container text-center">
            <div class="row">
                <div class="copyright">
                 		&copy; Copyright "T.A.D.D" 2017. Government of Rajasthan, All rights reserved.
                </div>
            </div>
        </div>
    </footer>

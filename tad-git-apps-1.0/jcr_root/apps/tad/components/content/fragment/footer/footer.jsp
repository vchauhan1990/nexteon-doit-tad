<%@include file="/apps/tad/common/global.jsp"%>

<c:set var="footerType">
    ${properties.footerType}
</c:set>
<c:if test="${empty footerType}">
    <c:set var="footerType" value="brandfooter"/>
</c:if>
<c:choose>
    <c:when test="${isEdit && empty footerType}">
        Please configure the footer component
    </c:when>
    <c:otherwise>
        <cq:include script="${footerType}.jsp" />
    </c:otherwise>
</c:choose>

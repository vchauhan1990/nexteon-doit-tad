<%@include file="/apps/tad/common/global.jsp" %>
 <c:set var="pageURL" value="${base:getMappedUrl(slingRequest, currentPage.path)}" />
    <header id="header-wrap">
        <div class="header">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
                    <cq:include path="logo" resourceType="tad/components/content/content/logo" />
                </div>
                <div class="col-xs-12 col-sm-12 col-md-7 col-lg-7">
                    <div class="signup-wrap">
                        <ul>
                            <li><a href="#">Sign up</a></li>
                            <li><a href="#">Log in</a></li>
                        </ul>
                    </div>
                    <ul class="top-link">
                        <li><a href="#scroll-1" class="skip-link">Skip to main content</a></li>
                        <li>
                            <div class="font-incr-decr">
                                <a href="#" id="font-in" title="Increase Font Size">A+</a>
                                <a href="#" id="font-df" title="Default Font Size">A</a>
                                <a href="#" id="font-dec" title="Decrease Font Size">A-</a>
                            </div>
                        </li>
                        <li>
                            <div class="theme-wrap">
                                <a href="#" class="theme-1" title="Black Theme" data-bg="#000" data-fg="#fff" id="color-theme-dark"></a>
                                <a href="#" class="theme-2" title="Red Theme" data-bg="#d9177f" data-fg="#fff" id="color-theme-light"></a>
                                <a href="#" class="theme-3" title="Default Theme" data-bg="#334777" data-fg="#555" id="color-theme-default"></a>
                            </div>
                        </li>
                        <li>
                             <cq:include path="search" resourceType="tad/components/content/content/search" />  
                        </li>
                        <li>
                            <a href="#" class="hamberg">MENU<i class="fa fa-bars"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </header>
    <div class="overlay-left"></div>        
    <div class="menu-wrap">        
        <div class="menu-header">
            <span class="nav-hd">Navigation</span>
            <span id="nav-close" class="glyphicon glyphicon-remove-circle"></span>
        </div>
        <div id="nav">
               <ul>
                 <cq:include path="navigation" resourceType="tad/components/content/content/navigation" />                            
            </ul>
        </div>        
    </div>




    <div class="overlay-right"></div>

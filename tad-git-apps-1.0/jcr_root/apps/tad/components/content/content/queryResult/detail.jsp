<%@include file="/apps/tad/common/global.jsp" %>
<base:adaptTo adaptable="${slingRequest}"  var="model"
    adaptTo="com.nexteonsolutions.aembootstrap.core.models.search.ArticleQuerySearchModel" />

<c:set var="type">
    <cq:text property="type" placeholder="" default="" />
</c:set>

<c:set var="heading">
    <cq:text property="heading" placeholder="" default="" />
</c:set>

<c:if test="${empty heading}">
    <c:set var="heading" value="${type}" />
</c:if>
<c:set var="text">
    <cq:text property="./text" escapeXml="true" placeholder="" default="" />
</c:set>
<c:set var="href">
    <cq:text property="url" placeholder="#" default="#" />
</c:set>
 <c:set var="url" value="${base:getMappedUrl(slingRequest, href)}" />
<h5>${heading}</h5>
<div class="news-press-list query-list">
    <ul>
     <c:forEach var="item" items="${model.resultList}">
         <li>
               <span>${item.date}</span>
              <h5><a href="${link}">${item.title}</a></h5>
                <c:if test="${not empty item.imagePath}">
                <c:url var="imageSrc" value="${item.imagePath}"/>
                    <figure class="query-image"> <img class="img-thumbnail" src="${imageSrc}"></figure>
                </c:if> 
              <c:choose>
                    <c:when test="${fn:length(item.description) > 250}">
                        <c:set var="description" value="${fn:substring(item.description, 0, 250)}..." />    
                        ${description} 
                    </c:when>    
                    <c:otherwise>
                        <p>${item.description}...</p>
                    </c:otherwise>    
                </c:choose>
             <a href="${base:getMappedUrl(slingRequest, item.path)}" class="btn btn-default">Read More</a>
         </li>
    </c:forEach>
    </ul>
     <c:set var="pagePath" value="${base:getMappedUrl(slingRequest, currentPage.path)}" />
    <a href="${base:getMappedUrl(slingRequest, url)}" class="more-btn">${text}</a>
</div>

        <c:if test="${fn:length(model.results.resultPages) > 1}">
            <div class="pagination-wrap" style="float: left;">   
                <nav>
                    <ul class="pagination">
                        <c:if test="${model.results.previousPage != null}">
                            <li>
                                <a href="${pagePath}.html?i=${model.results.previousPage.index}" aria-label="Previous">
                                    <span aria-hidden="true">&laquo;</span>
                                </a>
                            </li>
                        </c:if>
                        <c:forEach var="page" items="${model.results.resultPages}">
                            <li>
                                <a href="${pagePath}.html?i=${page.start}">
                                    ${page.index + 1}
                                </a>
                            </li>    
                        </c:forEach>
                        <c:if test="${model.results.nextPage != null}">
                            <li>
                                <a href="${pagePath}.html?i=${model.results.nextPage.start}" aria-label="Next">
                                    <span aria-hidden="true">&raquo;</span>
                                </a>
                            </li>
                        </c:if>
                    </ul>   
                </nav>    
            </div>        
        </c:if>
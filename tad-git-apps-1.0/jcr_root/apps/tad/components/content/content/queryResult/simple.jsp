<%@include file="/apps/tad/common/global.jsp" %>

<base:adaptTo adaptable="${slingRequest}"  var="model"
    adaptTo="com.nexteonsolutions.aembootstrap.core.models.search.ArticleQuerySearchModel" />

<c:set var="type">
	<cq:text property="type" placeholder="" default="" />
</c:set>

<c:set var="heading">
	<cq:text property="heading" placeholder="" default="" />
</c:set>

<c:set var="readMore">
	<cq:text property="readMore" placeholder="#" default="#" />
</c:set>

<c:if test="${empty heading}">
    <c:set var="heading" value="${type}" />
</c:if>
<c:set var="text">
	<cq:text property="./text" escapeXml="true" placeholder="" default="" />
</c:set>
<c:set var="href">
    <cq:text property="url" placeholder="#" default="#" />
</c:set>
 <c:set var="url" value="${base:getMappedUrl(slingRequest, href)}" />
<h5>${heading}</h5>
<div class="news-press-list query-list">
    <ul>
     <c:forEach var="item" items="${model.resultList}">

      <li>
          <c:set var="link" value="${base:getMappedUrl(slingRequest, item.path)}" />
          <p><a href="${link}">${item.title}</a></p>
              <span>${item.date}</span>
    </li>

   </c:forEach>
    </ul>
    <a href="${base:getMappedUrl(slingRequest, url)}" class="more-btn">${text}</a>
</div>


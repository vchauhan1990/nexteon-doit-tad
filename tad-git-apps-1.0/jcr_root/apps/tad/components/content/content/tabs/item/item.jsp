<%@include file="/apps/tad/common/global.jsp" %>

<c:set var="title">
    <cq:text property="title" placeholder="" default="" />
</c:set>

<c:choose>
    <c:when test="${not isDisabled && empty title}">
        Tab Name?
    </c:when>
    <c:when test="${not empty title}">
        <a href="#${base:hashCode(resource.path)}">${title}</a>
    </c:when>
</c:choose>

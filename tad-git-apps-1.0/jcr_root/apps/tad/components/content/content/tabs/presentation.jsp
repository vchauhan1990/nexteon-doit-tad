<%@include file="/apps/tad/common/global.jsp" %>

<c:set var="containerResource" value="${base:getResourceFromPath(slingRequest, containerResourcePath)}" />
<sling:getResource base="${resource}" path="items-par" var="containerResource" />

<sling:listChildren resource="${containerResource}" var="tabs" />

<c:choose>
    <c:when test="${isEdit && empty tabs}">
        Please configure the tabs module
    </c:when>
    <c:when test="${not empty tabs}">
        <div class="headline" id="tabsModule">
            <ul class="tabLinks">
                <c:forEach var="tab" items="${tabs}" varStatus="counter">
                    <li><sling:include path="${tab.path}" /></li>
                </c:forEach>
            </ul>
            <sling:listChildren resource="${containerResource}" var="tabs" />
            <c:forEach var="tab" items="${tabs}" varStatus="counter">
                <div id="${base:hashCode(tab.path)}" class="tabs-content">
                    <cq:include path="${tab.path}/links" resourceType="tad/components/content/content/linklist" />
                </div> 
            </c:forEach>   
        </div>     
    </c:when>
</c:choose>
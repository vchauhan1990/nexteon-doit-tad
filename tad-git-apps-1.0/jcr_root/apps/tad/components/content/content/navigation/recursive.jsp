<%@include file="/apps/energy/common/global.jsp" %>

<c:set var="listart" value="<li>" />
<c:set var="liend" value="</li>" /> 
<c:set var="depth" value='${depth + 1}' scope="request" />
<c:if test="${depth == 1}">
    <c:set var="divstart" value='<div class="column">' />
    <c:set var="divend" value="</div>" />
    <c:set var="h3start" value="<h3>" />
    <c:set var="h3end" value="</h3>" /> 
    <c:set var="listart" value="" />
    <c:set var="liend" value="" /> 
</c:if>

     <c:remove var="ulclass"/>
<c:forEach var="child" items="${recurChildren}">
    <sling:getResource base="${resource}" path="${child.path}" var="contentRes" />
    <c:set var="path" value="${fn:replace(child.url, '.html', '')}" />
    <c:set var="path" value="${base:resolvePath(slingRequest, path)}" />
    <c:set var="urlPath" value="${currentPage.path}" />
    <c:set var="classActive" value="" />
    <c:set var="classMenuOpen" value="" />
    <c:set var="title"><c:out value="${base:applySubstitution(slingRequest, child.navigationTitle)}" escapeXml="true" /></c:set>
    <c:choose>
        <c:when test="${fn:length(child.children) > 0}">

            <li>
                <a href="#"  title="${title}">${title}</a>
                <c:set var="recurChildren" value="${child.children}" scope="request" />
                <c:set var="counter" value="${counter + 1}" scope="request" />
                <ul>
                    <jsp:include page="${resource.path}.recursive.html" />
                </ul>
            </li>
        </c:when>


        <c:otherwise>
            <li>
                <c:set var="new" value="${child.path}/jcr:content"/>
                <c:set var="content" value="${sling:getResource(resourceResolver,new)}" />
                <c:set var="newTab"  value="${content.valueMap['openInNewTab']}"/>

                <c:choose>
                    <c:when test="${newTab eq 'true'}">
                        <a href="${base:getMappedUrl(slingRequest, child.url)}" target="_blank" title="${title}">${title}</a>
                    </c:when>
                    <c:otherwise>
                        <a href="${base:getMappedUrl(slingRequest, child.url)}" title="${title}">${title}</a>
                    </c:otherwise>
                </c:choose>
            </li>
        </c:otherwise>
    </c:choose>

</c:forEach>

<c:set var="depth" value='${depth - 1}' scope="request" />
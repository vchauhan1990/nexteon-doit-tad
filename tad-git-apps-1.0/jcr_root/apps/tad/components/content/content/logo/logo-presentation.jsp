<%@include file="/apps/tad/common/global.jsp" %>

<c:set var="title">
    <cq:text property="title" placeholder="" default="" />
</c:set>

<c:set var="text">
    <cq:text property="text" escapeXml="true" placeholder="" default="" />
</c:set>
<c:set var="href">
    <cq:text property="target" placeholder="#" default="#" />
</c:set>

<c:set var="imageReference">
    <cq:text property="image/fileReference" placeholder=""  default="" />
</c:set>


<figure class="logo">
    <a href="${base:getMappedUrl(slingRequest, href)}">
        <img src='${imageReference}' alt="">
        <small>
            <span class="h3-hd">Government of Rajasthan</span>
            <span class="h2-hd">${title}</span>
        </small>
    </a>
</figure>


<%@include file="/apps/forest/common/global.jsp" %>

<c:set var="title">
    <cq:text property="title" placeholder="" default="" />
</c:set>
<c:set var="subtitle">
    <cq:text property="subtitle" placeholder="" default="" />
</c:set>
<c:set var="description">
    <cq:text property="description" placeholder="" default="" />
</c:set>
<c:set var="href">
    <cq:text property="url" placeholder="" default="" />
</c:set>
<c:if test="${not empty href}">
    <c:set var="href" value="${base:getMappedUrl(slingRequest, href)}" />
    <c:set var="linklabel">
        <cq:text property="linklabel" placeholder="Read More" default="Read More" />
    </c:set>    
</c:if>
<c:set var="imageReference">
    <cq:text property="image/fileReference" placeholder="" default="" />
</c:set>
<c:if test="${not empty imageReference}">
	<c:url var="imageReferenceUrl" value="${imageReference}" />
</c:if>
<%@include file="/apps/forest/common/global.jsp" %>

<%@include file="init.jsp" %>

<c:choose>
	<c:when test="${not isDisabled && empty title}">
        Please configure the teaser
    </c:when>
	<c:when test="${not empty title}">
        <c:set var="styleBackGround" value="background: url(${imageReferenceUrl});" />
        <div class="box-content" style="${styleBackGround}">
            <div class="box-overlap">
                <p class="golden-text">${title}</p>
                <h4><a title="${title}" href="${href}">${subtitle}</a></h4>
                ${description}
                <a href="${href}" class="read-more">${linklabel}</a>
            </div>
        </div>
	</c:when>
</c:choose>
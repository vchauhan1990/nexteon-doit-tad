<%@include file="/apps/forest/common/global.jsp" %>

<%@include file="init.jsp" %>

<c:choose>
	<c:when test="${not isDisabled && empty title}">
        Please configure the teaser
    </c:when>
	<c:when test="${not empty title}">
		 <div class="header-banner">
			<img src="${imageReferenceUrl}" alt="${title}">
		    <div class="overlay alpha-55">
		        <div class="overlay-title">
		            <span class="title-first">${title}</span>
		            <span class="title-bottom">${subtitle}</span>
		        </div>
		    </div>
		</div> 
	</c:when>
</c:choose>



<%@include file="/apps/tad/common/global.jsp" %>

<c:set var="containerResource" value="${base:getResourceFromPath(slingRequest, containerResourcePath)}" />
<sling:getResource base="${resource}" path="items-par" var="containerResource" />

<sling:listChildren resource="${containerResource}" var="slides" />
<div class="video-slider">

    <c:choose>
    <c:when test="${isEdit && empty slides}">
        Please configure the video slider
    </c:when>
    <c:when test="${not empty slides}">    

            <c:forEach var="slide" items="${slides}" varStatus="counter">



                    <c:if test="${counter.count eq '1'}">
                        <c:set var="activeClass" value="active"/>
                    </c:if>
                	<c:if test="${counter.count ne '1'}">
                        <c:set var="activeClass" value=""/>
                    </c:if>


						<sling:include path="${slide.path}" />



            </c:forEach>

    </c:when>
</c:choose>
</div>
<%@include file="/apps/tad/common/global.jsp" %>

<c:set var="title">
    <cq:text property="title" placeholder="" default="" />
</c:set>

<c:set var="asset">
    <cq:text property="./asset" placeholder="" default="" />
</c:set>

<c:set var="href">
    <cq:text property="url" placeholder="" default="" />
</c:set>
<c:if test="${not empty href}">
    <c:set var="href" value="${base:getMappedUrl(slingRequest, href)}" />

</c:if>

<c:set var="imageReference">
    <cq:text property="image/fileReference" placeholder="" default="" />
</c:set>


<c:choose>
    <c:when test="${not isDisabled && empty asset}">
         <img class="cq-dd-image cq-image-placeholder">

    </c:when>
    <c:when test="${not empty asset}">
    <div class="item">
	   <div class="col-wrap anicurve">
        <h3><a href="${href}">${title}</a></h3>
          <div class="slider-video-wrap">


		      <video  poster="${assetUrl}" controls>
              <source src="${asset}" type="video/mp4">

			Your browser does not support the video tag.
		  </video>
           </div>
         </div>

           </div>

    </c:when>
</c:choose>



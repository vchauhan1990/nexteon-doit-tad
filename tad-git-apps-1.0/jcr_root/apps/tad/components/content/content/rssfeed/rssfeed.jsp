<%@include file="/apps/aembootstrap/common/global.jsp"%>

<base:adaptTo adaptable="${slingRequest}"  var="model"
adaptTo="com.nexteonsolutions.aembootstrap.core.models.rssfeed.RSSFeedConsumerModel" />

<cq:includeClientLib categories="tad.rssfeed"/>

<c:set var="rssFeedUrl">
    <cq:text property="rssUrl" placeholder="#"/>
</c:set>

<c:choose>
    <c:when test="${ empty rssFeedUrl && isAuthor}">
        <p>Please configure Url for Rss Feed</p>
    </c:when>    
    <c:when test="${not empty rssFeedUrl && isPublish}">
        <div class="feed-slider">
            
            <c:forEach items="${model.rssFeedItemsList}" var="element"> 
                <div class="item">
                    
                    <h4><a href="${element.link}" target="_blank">${element.title}</a></h4>    
                    <span>${element.pubDate}</span>
                    <p>${element.description} </p>
                </div>
            </c:forEach>
        </div>    
    </c:when>    
    <c:otherwise>
        <p>Rss Feed component is configured</p>
    </c:otherwise>
</c:choose>    



<%@include file="/apps/tad/common/global.jsp"%>

<c:set var="url">
	<cq:text property="url" placeholder="#" default="#" />
</c:set>

<div class="search-wrap">
    <a href="#" class="search-btn">
        <i class="fa fa-search"></i>
        <i class="fa fa-times" aria-hidden="true"></i>
    </a>
   	<form id="bootstrap-gv-rmds-srch-frm" action="${url}.html">
		<input type="text" name="q" id="search" class="form-control" placeholder="Type Keywords & Hits Enter">
    </form> 
</div>
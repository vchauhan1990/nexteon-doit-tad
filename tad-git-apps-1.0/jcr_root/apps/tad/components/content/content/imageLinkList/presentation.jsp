<%@include file="/apps/tad/common/global.jsp" %>

<c:set var="containerResource" value="${base:getResourceFromPath(slingRequest, containerResourcePath)}" />
<sling:getResource base="${resource}" path="items-par" var="containerResource" />

<sling:listChildren resource="${containerResource}" var="slides" />

<c:choose>
    <c:when test="${isEdit && empty slides}">
        Please configure the image list
    </c:when>
    <c:when test="${not empty slides}">
<ul>
		<c:forEach var="slide" items="${slides}" varStatus="counter">
            <li>
		    <sling:include path="${slide.path}" />
            </li>
		</c:forEach>
        </ul>      
    </c:when>
</c:choose>

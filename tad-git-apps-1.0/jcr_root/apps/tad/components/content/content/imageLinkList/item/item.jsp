<%@include file="/apps/tad/common/global.jsp" %>

<c:set var="imageReference">
    <cq:text property="image/fileReference" placeholder="" default="" />
</c:set>

<c:set var="href">
    <cq:text property="url" placeholder="" default="" />
</c:set>

<c:set var="title">
    <cq:text property="title" placeholder="" default="" />
</c:set>

<c:choose>
    <c:when test="${not isDisabled && empty imageReference}">
        <img class="cq-dd-image cq-image-placeholder"/>
    </c:when>
    <c:when test="${not empty imageReference}">
        <c:set var="imageReferenceUrl" value="${base:encode(imageReference)}"/>
        <a title="${title}" href="${href}" target="_blank"><img src="${imageReferenceUrl}" alt="${title}"></a>
    </c:when>
</c:choose>
<%@include file="/apps/tad/common/global.jsp"%>

<c:set var="label">
    <cq:text property="text" placeholder="" default="" />
</c:set>
<c:set var="href">
    <cq:text property="url" placeholder="#" default="#" />
</c:set>
<c:set var="target">
    <cq:text property="target" placeholder="_self" default="_self" />
</c:set>
<c:set var="showIcon">
    <cq:text property="showIcon" placeholder="" default="" />
</c:set>
<c:set var="date">
    <cq:text property="date" placeholder="" default="" />
</c:set>
<c:set var="target">
    <cq:text property="target" placeholder="" default="" />
</c:set>
<c:if test="${not empty showIcon}">
	<c:set var="iconClass">
	    <cq:text property="icon" placeholder="" default="" />
	</c:set>
</c:if>

<c:choose>
	<c:when test="${not isDisabled && empty label}">
        Please configure the link component
    </c:when>
	<c:when test="${not empty label}">
		<c:set var="url" value="${base:getMappedUrl(slingRequest, href)}" />
		<c:set var="target">target="${target}"</c:set>
		<c:set var="dataHref" value="" />
		<c:set var="class" value="" />
		<c:if test="${target == 'lightbox'}">
			<c:set var="dataHref">
				data-href="${base:getMappedUrl(slingRequest, url)}"
            </c:set>
			<c:set var="class" value="lightbox-feature-open" />
			<c:set var="url" value="#" />
			<c:set var="target" value="" />
		</c:if>


        <a href="${base:getMappedUrl(slingRequest, url)}" ${target}>
            ${label}</a>
        <span>${date}</span>


	</c:when>
</c:choose>

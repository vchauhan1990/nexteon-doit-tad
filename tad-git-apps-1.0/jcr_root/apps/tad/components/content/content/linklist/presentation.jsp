<%@include file="/apps/tad/common/global.jsp"%>

<c:set var="containerResource" value="${base:getResourceFromPath(slingRequest, containerResourcePath)}" />
<sling:getResource base="${resource}" path="items-par" var="containerResource" />

<sling:listChildren resource="${containerResource}" var="links" />

<c:choose>
	<c:when test="${not isDisabled && empty links}">
        Please configure the link list
    </c:when>
	<c:when test="${not empty links}">

                <c:set var="ulstart" value="<ul>" />
                <c:set var="ulend" value="</ul>" />
                <c:set var="listart" value="<li>" />
                <c:set var="liend" value="</li>" />

            ${ulstart}
			<c:forEach var="link" items="${links}">
				${listart}
				<sling:include path="${link.path}" />
				${liend}
			</c:forEach>
			${ulend}
	</c:when>
</c:choose>

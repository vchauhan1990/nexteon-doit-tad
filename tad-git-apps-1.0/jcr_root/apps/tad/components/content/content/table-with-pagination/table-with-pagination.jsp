<%@include file="/apps/tad/common/global.jsp"%>

<c:set var="text">
    <cq:text property="tableData" escapeXml="false" placeholder="" default="" />
</c:set>
<c:set var="limit">
    <cq:text property="limit" placeholder="5" default="5" />
</c:set>
<c:set var="limitPerPage">
    <cq:text property="limitperpage"  placeholder="7" default="7"/>
</c:set>
<c:set var="color">
    <cq:text property="color"  placeholder="cacaca" default="cacaca" />
</c:set>
<c:set var="headerColor">
    <cq:text property="headercolor"  placeholder="4778BD" default="4778BD" />
</c:set>
<c:set var="newClass" value="<table class='table table-bordered table-striped'" />


<c:choose>
    <c:when test="${not isDisabled && empty text}">
        <img src="/libs/cq/ui/resources/0.gif" class="cq-table-placeholder" alt="">
    </c:when>
    <c:otherwise>

		<div class="table-striped ">
            <c:set var="newClass" value="<table class='table  udhtable ${resource.name} table-bordered table-striped custom-table'" />
            <c:set var="searchItem" value='<table'/>

                <c:set var="newTable" value="${fn:replace(text, searchItem , newClass)}" />
                ${newTable}

               <div class="recpage" id="${resource.name}"></div> 
            </div>

    </c:otherwise>
</c:choose>

<style>
    .${resource.name} > tbody > tr:nth-child(2n+1) > td, 
    .${resource.name} > tbody > tr:nth-child(2n+1) > th {
        background: #${color};

    }

    .${resource.name} > tbody > tr:nth-child(1) > td, 
    .${resource.name} > tbody > tr:nth-child(1) > th {
        background: #${headerColor};

    }
    #${resource.name}{
        width: 100%;
        padding: 0;
        margin: 0;
        padding: 10px 0 0;
        background-color: #${headerColor};
    }

</style> 


<script>

    $(document).ready(function(){
        $("#${resource.name}").smartpaginator({
            totalrecords: $(".${resource.name} tr").length,
            recordsperpage: ${limitPerPage},
            datacontainer:'${resource.name}',
            dataelement: 'tr',
            initval: 0,
            next: 'Next',
            prev: 'Prev',
            first: 'First',
            last: 'Last',
        });

    }); 


</script>  

<style type="text/css">


    #${resource.name} > table th, .custom-table table th{
            line-height:51px;
        }

   #${resource.name} > table th:first-child, .custom-table table th:first-child {
        width: 12%;

    }

   #${resource.name} > table th img, .custom-table table th img{
        float: left;
        width: 50px;
        height:50px;
        padding-right:10px;
    }
</style>

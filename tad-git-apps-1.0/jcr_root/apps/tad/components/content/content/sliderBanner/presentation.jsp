<%@include file="/apps/tad/common/global.jsp" %>

<c:set var="containerResource" value="${base:getResourceFromPath(slingRequest, containerResourcePath)}" />
<sling:getResource base="${resource}" path="items-par" var="containerResource" />

<sling:listChildren resource="${containerResource}" var="slides" />

<c:choose>
    <c:when test="${isEdit && empty slides}">
        Please configure the slider banner
    </c:when>
    <c:when test="${not empty slides}">    
	 <div class="four-colum-slider-wrap">
       <div class="four-colum-slider">
            <c:forEach var="slide" items="${slides}" varStatus="counter">
				<div class="item">
  					<div class="col-box">
                	<c:if test="${counter.count eq '1'}">
                        <c:set var="activeClass" value="active"/>
                    </c:if>
                	<c:if test="${counter.count ne '1'}">
                        <c:set var="activeClass" value=""/>
                    </c:if>
					<sling:include path="${slide.path}" />
    				</div>
           		</div>
			</c:forEach>
  		</div>
      </div>
    </c:when>
</c:choose>

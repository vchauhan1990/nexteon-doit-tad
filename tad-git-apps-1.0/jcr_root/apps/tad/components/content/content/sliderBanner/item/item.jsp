 <%@include file="/apps/tad/common/global.jsp" %>

<c:set var="text">
    <cq:text property="text" placeholder="" default="" />
</c:set>

<c:set var="description">
    <cq:text property="description" placeholder="" default="" />
</c:set>

<c:set var="buttontext">
    <cq:text property="buttontext" placeholder="Read More" default="Read More" />
</c:set>  
<c:set var="href">
    <cq:text property="url" placeholder="" default="" />
</c:set>

<c:if test="${not empty href}">
    <c:set var="href" value="${base:getMappedUrl(slingRequest, href)}" />
</c:if>
<c:choose>
    <c:when test="${not isDisabled && empty text && empty description}">
        <img class="cq-dd-image cq-image-placeholder">
    </c:when>
    <c:when test="${not empty text && not empty description}">

                    <span class="icon-img"><i class="fa fa-cog" aria-hidden="true"></i></span>
                    <h3>${text}</h3>
                    <p>${description}</p>
                    <a href="${href}" class="ctn black">${buttontext}</a>
     </c:when>
</c:choose>





 <%@include file="/apps/tad/common/global.jsp" %>

<c:set var="title">
    <cq:text property="title" placeholder="" default="" />
</c:set>
<c:set var="description">
    <cq:text property="description" placeholder="" default="" />
</c:set>
<c:set var="href">
    <cq:text property="url" placeholder="" default="" />
</c:set>
<c:if test="${not empty href}">
    <c:set var="href" value="${base:getMappedUrl(slingRequest, href)}" />
    <c:set var="linklabel">
        <cq:text property="linklabel" placeholder="Read More" default="Read More" />
    </c:set>    
</c:if>
<c:set var="imageReference">
    <cq:text property="image/fileReference" placeholder="" default="" />
</c:set>

<c:choose>
    <c:when test="${not isDisabled && empty title}">
        Please configure the slide
    </c:when>
    <c:when test="${not empty title}">
         <c:if test="${not empty imageReference}">
           <c:url var="imageReferenceUrl" value="${imageReference}" />
        </c:if>
                    <div class="item">
                        <figure class="slide-img">
                            <img src="${imageReferenceUrl}" alt="slider1">
                        </figure>
                        <div class="slider-content">
                            <div class="container">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-md-offset-4 col-lg-offset-4">
                                        <h2>${title}</h2>
                                         ${description}
                                        <a href="${href}" class="ctn">Readmore</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

    </c:when>
</c:choose>
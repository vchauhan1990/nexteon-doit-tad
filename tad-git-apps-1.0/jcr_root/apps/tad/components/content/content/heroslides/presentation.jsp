<%@include file="/apps/tad/common/global.jsp" %>

<c:set var="containerResource" value="${base:getResourceFromPath(slingRequest, containerResourcePath)}" />
<sling:getResource base="${resource}" path="items-par" var="containerResource" />

<sling:listChildren resource="${containerResource}" var="slides" />

 <div id="heroSlider">
<c:choose>
    <c:when test="${isEdit && empty slides}">
        Please configure the hero slides
    </c:when>
    <c:when test="${not empty slides}">    


            <c:forEach var="slide" items="${slides}" varStatus="counter">


                   <c:if test="${counter.count eq '1'}">
                        <c:set var="activeClass" value="active"/>
                    </c:if>
                    <c:if test="${counter.count ne '1'}">
                        <c:set var="activeClass" value=""/>
                    </c:if>
                    <div class="item ${activeClass}" >
                        <sling:include path="${slide.path}" />
                    </div>


            </c:forEach>
 
     </c:when>
</c:choose>
</div>


 
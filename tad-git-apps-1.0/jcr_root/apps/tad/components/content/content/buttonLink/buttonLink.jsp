<%@include file="/apps/tad/common/global.jsp"%>
<c:set var="text">
    <cq:text property="text" placeholder="" default="" />
</c:set>
<c:set var="href">
    <cq:text property="url" placeholder="" default="#" />
</c:set>
<c:choose>
    <c:when test="${not isDisabled && empty text}">
        Please configure the button
    </c:when>
    <c:when test="${not empty text}">
        	<a href="${href}" target="_blank" class="ctn black">${text}</a>
    </c:when>
</c:choose>

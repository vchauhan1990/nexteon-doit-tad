<%@include file="/apps/tad/common/global.jsp" %>
<c:set var="title">
    <cq:text property="title" placeholder="" default="" />
</c:set>
<c:set var="description">
    <cq:text property="description" placeholder="" default="" />
</c:set>
<c:set var="articlePage" value="${base:getContainingPage(pageManager, resource.path)}" />
<c:if test="${not empty articlePage}">
    <c:set var="href" value="${base:getMappedUrl(slingRequest, articlePage.path)}" />
    <c:set var="linktext">
        <cq:text property="linktext" placeholder="Read More" default="Read More" />
    </c:set>    
</c:if>
<c:set var="imageReference">
    <cq:text property="image/fileReference" placeholder="" default="" />
</c:set>

<c:set var="articleDate">
    <cq:text property="date" placeholder="" default="" />
</c:set>
<c:set var="fileName">
    <cq:text property="fileName" placeholder="" default="" />
</c:set>
<c:set var="fileReference">
    <cq:text property="fileReference" placeholder="" default="" />
</c:set>


<section class="rightContainer orange-light">
    <div class="block">
        <div class="gallery-block">
            <h1>${currentPage.title}</h1>
            <div class="gallery-top">


                    <c:forEach var="childPage" items="${base:getChildPages(currentPage)}" varStatus="counter">
                        <sling:getResource base="${childPage.contentResource}" path="detail" var="detail" />
                        
                        <div class="row content-wrap">

                        <c:if test="${fn:length(detail.valueMap['description']) > 100}">
                            <c:set var="description" value="${fn:substring(detail.valueMap['description'], 0, 100)}..." />  
                        </c:if>  
                        <c:choose>
                            
                            <c:when test="${not empty detail.valueMap['image/fileReference']}">
                                <c:set var="url" value="${base:getMappedUrl(slingRequest, href)}" />

                                
                                    <div class="col-md-3 col-xs-12">
                                    <img src="${detail.valueMap['image/fileReference']}" alt="${title}" class="pull-left">
                                    </div>
                                    <div class="col-md-9 col-xs-12">
                                                                                                 
                                        <h2><a href="${url}">${detail.valueMap['title']}</a></h2>

                                              <span class="date">
                                               <fmt:formatDate pattern="d MMMMM, yyyy" value="${detail.valueMap['date'].time}"/>
                                              </span> 

                                        
                                        ${description} <br>
                                       <c:if test="${empty detail.valueMap['linktext']}">
                                      <a href="${childPage.path}.html"><button class="btn btn-default">Read More</button></a>
                                    </c:if>
                                    <c:if test="${not empty detail.valueMap['linktext']}">
                                     <a href="${childPage.path}.html" class="btn btn-default">${detail.valueMap['linktext']}</a>
                                      </c:if>
                                    </div>
                                
                            </c:when>
                            <c:otherwise>
                                <c:set var="url" value="${base:getMappedUrl(slingRequest, href)}" />
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    
                                    <h2><a href="${url}">${detail.valueMap['title']}</a></h2>

                                        
                                       <span class="date">
                                               <fmt:formatDate pattern="d MMMMM, yyyy" value="${detail.valueMap['date'].time}"/>
                                              </span> 

                                          ${description}<br>
                                    <c:if test="${empty detail.valueMap['linktext']}">
                                      <a href="${childPage.path}.html"><button class="btn btn-default">Read More</button></a>
                                    </c:if>
                                    <c:if test="${not empty detail.valueMap['linktext']}">
                                     <a href="${childPage.path}.html" class="btn btn-default">${detail.valueMap['linktext']}</a>
                                      </c:if>

                                </div>
                            </c:otherwise>
                        </c:choose>  
                    </div>

                    
               
                
            </c:forEach>    
        </div>
      </div>  
    </div>
</section>

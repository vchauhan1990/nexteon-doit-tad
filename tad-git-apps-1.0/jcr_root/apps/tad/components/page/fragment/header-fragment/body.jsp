<%@include file="/apps/tad/common/global.jsp" %>

<body>
	<!--[if lt IE 7]>
	<p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
	<![endif]-->
    <div class="container-fluid main">
		<cq:include script="content.jsp" />
    </div>
	<cq:include script="footerlibs.jsp"/>
</body>
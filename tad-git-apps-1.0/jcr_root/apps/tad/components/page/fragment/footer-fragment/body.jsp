<%@include file="/apps/tad/common/global.jsp" %>

<body>
	<!--[if lt IE 7]>
	<p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
	<![endif]-->
    <h1>tad footer</h1>
	<cq:include script="content.jsp" />
	<cq:include script="footerlibs.jsp"/>
</body>
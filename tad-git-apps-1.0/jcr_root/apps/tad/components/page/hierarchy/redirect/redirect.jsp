<%@include file="/apps/tad/common/global.jsp" %>


<base:adaptTo adaptable="${slingRequest}"  var="model"
    adaptTo="com.nexteonsolutions.aembootstrap.core.models.pageredirect.RediectPageModel" />


<c:choose>
    <c:when test="${isDisabled}">
        ${model.redirectMandate}
    </c:when>
    <c:otherwise>
        <sling:include replaceSelectors="redirectinauthor" />
    </c:otherwise>
</c:choose>

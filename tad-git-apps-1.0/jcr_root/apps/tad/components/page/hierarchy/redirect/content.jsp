<%@include file="/apps/tad/common/global.jsp" %>

<base:adaptTo adaptable="${slingRequest}"  var="model"
    adaptTo="com.nexteonsolutions.aembootstrap.core.models.pageredirect.RediectPageModel" />
    
This page redirects <a href="${model.absoluteRedirectUrl}">Here</a>

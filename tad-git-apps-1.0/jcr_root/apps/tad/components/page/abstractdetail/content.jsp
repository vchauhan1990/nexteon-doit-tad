<%@include file="/apps/tad/common/global.jsp" %>

<section class="rightContainer orange-light">
    <div class="block">
        <div class="gallery-block">
            <h2>${currentPage.title}</h2>
            <div class="gallery-top">
                <cq:include path="content-par" resourceType="foundation/components/parsys" />
                <cq:include path="detail" resourceType="tad/components/content/content/articledetail" />
            </div>
        </div>
    </div>
</section>

<%@include file="/apps/tad/common/global.jsp" %>

<div class="overlay-right"></div>
<main class="main-container">

 <section class="heroSliderWrap">
     <div class="heroSlider">
     <cq:include path="banner-image" resourceType="tad/components/content/content/heroslides"/>
     </div>
     		<div class="cm-img">
                <figure>
                    <cq:include path="cm-img" resourceType="tad/components/content/content/image"/>
                </figure>
            </div>
            <div class="scroll-button">
                <a href="#section-1"><i class="fa fa-arrow-circle-o-down"></i></a>
            </div>

  </section>



<!--Section Below Banner -->
<section class="colum-wrap" id="section-1">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="two-colum-top">
                            <h2><cq:include path="introduction-heading" resourceType="tad/components/content/content/text"/></h2>
                            <p><cq:include path="introduction-text" resourceType="tad/components/content/content/richtext"/></p>
                        </div>
                    </div>
                </div>
                <div class="row two-colum-bottom">
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                        <figure class="two-colum-img">
                            <cq:include path="introduction-image" resourceType="tad/components/content/content/image"/>
                        </figure>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                        <h3><cq:include path="introduction-subheading" resourceType="tad/components/content/content/text"/></h3>
                        <p><cq:include path="introduction-subtext" resourceType="tad/components/content/content/richtext"/></p>
                        <cq:include path="introduction-button" resourceType="tad/components/content/content/buttonLink"/>
                    </div>
                </div>
            </div>
		    <div class="scroll-button">
                <a href="#section-2"><i class="fa fa-arrow-circle-o-down"></i></a>
            </div>
</section>
<!--End of Section Below Banner-->

<!--Three Block Section-->

<section id="section-2">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                        <div class="col-box">
                            <span class="icon-img"><i class="fa fa-trophy" aria-hidden="true"></i></span>
                            <h3><cq:include path="three-block-text1" resourceType="tad/components/content/content/text"/></h3>
                            <p><cq:include path="three-block-richtext1" resourceType="tad/components/content/content/richtext"/></p>
                            <cq:include path="three-block-button" resourceType="tad/components/content/content/buttonLink"/>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                        <div class="col-box">
                            <span class="icon-img"><i class="fa fa-bullhorn" aria-hidden="true"></i></span>
                            <h3><cq:include path="three-block-text2" resourceType="tad/components/content/content/text"/></h3>
                            <p><cq:include path="three-block-richtext2" resourceType="tad/components/content/content/richtext"/></p>
                            <cq:include path="three-block-button" resourceType="tad/components/content/content/buttonLink"/>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                        <div class="col-box">
                            <span class="icon-img"><i class="fa fa-newspaper-o" aria-hidden="true"></i></span>
                            <h3><cq:include path="three-block-text3" resourceType="tad/components/content/content/text"/></h3>
                            <p><cq:include path="three-block-richtext3" resourceType="tad/components/content/content/richtext"/></p>
                            <cq:include path="three-block-button" resourceType="tad/components/content/content/buttonLink"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="scroll-button">
                <a href="#section-3"><i class="fa fa-arrow-circle-o-down"></i></a>
            </div>
</section>

<!--End of Three Block Section-->

<!-- Slider-Banner -->
<section class="widget-wrap" id="section-3">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="two-colum-top">
                            <h2><cq:include path="slider-banner-heading" resourceType="tad/components/content/content/text"/></h2>
                            <p><cq:include path="slider-banner-subheading" resourceType="tad/components/content/content/richtext"/></p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

								<cq:include path="slider-banner-boxes" resourceType="tad/components/content/content/sliderBanner"/>
                            </div>
                        </div>
                    </div>
            <div class="scroll-button">
                <a href="#section-4"><i class="fa fa-arrow-circle-o-down"></i></a>
            </div>
        </section>

<!-- End of slider banner -->


<!--Latest Updates-->

<section class="colum-wrap" id="section-4">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="two-colum-top">
                            <h2><cq:include path="latest-update-heading" resourceType="tad/components/content/content/text"/></h2>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 text-center">
                        <div class="latest-updates-wrap">
                            <figure class="latest-update-img">
                                <cq:include path="latest-update-news" resourceType="tad/components/content/content/image"/>
                            </figure>
                            <h2><cq:include path="latest-update-text1" resourceType="tad/components/content/content/text"/></h2>
                            <div class="latest-updates-content">
                                <p><cq:include path="latest-update-richtext1" resourceType="tad/components/content/content/richtext"/></p>
                                <cq:include path="latest-update-button1" resourceType="tad/components/content/content/buttonLink"/>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 text-center">
                        <div class="latest-updates-wrap">
                            <figure class="latest-update-img">
                               <cq:include path="latest-update-events" resourceType="tad/components/content/content/image"/>
                            </figure>
                            <h2><cq:include path="latest-update-text2" resourceType="tad/components/content/content/text"/></h2>
                            <div class="latest-updates-content">
                                <p><cq:include path="latest-update-richtext2" resourceType="tad/components/content/content/richtext"/></p>
                                <cq:include path="latest-update-button2" resourceType="tad/components/content/content/buttonLink"/>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 text-center">
                        <div class="latest-updates-wrap">
                            <figure class="latest-update-img">
                                <cq:include path="latest-update-tender" resourceType="tad/components/content/content/image"/>
                            </figure>
                            <h2><cq:include path="latest-update-text3" resourceType="tad/components/content/content/text"/></h2>
                            <div class="latest-updates-content">
                                <p><cq:include path="latest-update-richtext3" resourceType="tad/components/content/content/richtext"/></p>
                                <cq:include path="latest-update-button3" resourceType="tad/components/content/content/buttonLink"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="scroll-button">
                <a href="#section-5"><i class="fa fa-arrow-circle-o-down"></i></a>
            </div>
        </section>


<!--End of latest updates-->    

 <!-- Section Above Footer-->
    <section class="colum-wrap" id="section-5">
        <div class="help-line">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-md-12 text-center">
                        <h2><cq:include path="section-helpline" resourceType="tad/components/content/content/text"/></h2>
                        <p><cq:include path="section-helpline-subtext" resourceType="tad/components/content/content/text"/></p>
                        <a href="#"><cq:include path="section-helpline-box-richtext" resourceType="tad/components/content/content/richtext"/></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="scroll-button">
                <a href="#section-6"><i class="fa fa-arrow-circle-o-down"></i></a>
            </div>
    </section>
    <!-- end help-line -->
    <section class="colum-wrap" id="section-6">
        <div class="sub-footer">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="partners-logo">


                              <cq:include path="partner-image" resourceType="tad/components/content/content/imageLinkList"/>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<!--End of the Section -->
</main>
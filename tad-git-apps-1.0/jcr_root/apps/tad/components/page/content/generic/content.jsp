<%@include file="/apps/tad/common/global.jsp" %>

<div class="overlay-right"></div>
<div style="margin-top:130px;"></div>
<main class="main-container">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<div class="two-colum-top">
              		<h2>${currentPage.title}</h2>
              		<cq:include path="content-par" resourceType="foundation/components/parsys" />
        		</div>
            	</div>
         	</div>
     	</div>
</main>
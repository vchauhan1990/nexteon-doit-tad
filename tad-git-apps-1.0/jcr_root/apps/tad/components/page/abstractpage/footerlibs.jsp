<%@include file="/apps/tad/common/global.jsp" %>
<%
	com.day.cq.commons.inherit.InheritanceValueMap  valueMap = new com.day.cq.commons.inherit.HierarchyNodeInheritanceValueMap(resource);
	String imgSrc = valueMap.getInherited("cmimage/fileReference", "/content/dam/doitassets/common/images/cm/cm-rajasthan.png");
%>

<c:choose>
    <c:when test="${not isPublish}">
        <cq:includeClientLib js="${currentDesign.id}.author"/>
    </c:when>
    <c:otherwise>
        <cq:includeClientLib js="${currentDesign.id}.publish"/>
    </c:otherwise>
</c:choose>

<script>
    window.onload = function() {
        if ($('body').hasClass("stateCM")) {

            var setimg = "<%= imgSrc %>";
            $(".cmimg img").attr("src", setimg);
        }
    };
</script>

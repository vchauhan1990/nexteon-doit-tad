<%@include file="/apps/tad/common/global.jsp" %>
<link rel="icon" type="image/png" href="/etc/designs/tad/clientlib/core/images/favicon.ico">
<c:choose>
    <c:when test="${not isPublish}">
        <cq:includeClientLib css="${currentDesign.id}.author"/>
    </c:when>
    <c:otherwise>
        <cq:includeClientLib css="${currentDesign.id}.publish"/>
    </c:otherwise>
</c:choose>

<cq:include script="moreheadlibs.jsp" />